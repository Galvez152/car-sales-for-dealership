from django.http import JsonResponse
from .models import AutomobileVO, Technician, Service
from .encoders import TechnicianEncoder, ServiceEncoder
from django.views.decorators.http import require_http_methods
import json

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(technician, encoder = TechnicianEncoder, safe=False)
        except:
            return JsonResponse(
                {"message": "Could not add technician"}, status=400
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Technician not found"}, status = 400)
    if request.method == "GET":
        return JsonResponse(
            technician, encoder = TechnicianEncoder, safe=False
        )
    elif request.method =="PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician, encoder = TechnicianEncoder, safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services}, encoder=ServiceEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            vin = AutomobileVO.objects.get(vin = content['vin'])
            content['vin'] = vin
            technician = Technician.objects.get(name = content['technician'])
            content['technician'] = technician
            service = Service.objects.create(**content)
            response = JsonResponse(
                service, encoder = ServiceEncoder, safe=False
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Vehicle does not exist"}, status=400
            )
        return response
        

@require_http_methods(["GET", "PUT", "DELETE"])
def api_service(request, pk):
    try:
        service = Service.objects.get(id=pk)
    except Service.DoesNotExist:
        return JsonResponse({"message": "Service not found"}, status=400)
    if request.method == "GET":
        return JsonResponse(
            service, encoder = ServiceEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            vin = AutomobileVO.objects.get(vin = content['vin'])
            content['vin'] = vin
            technician = Technician.objects.get(name = content['technician'])
            content['technician'] = technician
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Vehicle does not exist"}, status=400)
        Service.objects.filter(id=pk).update(**content)
        service = Service.objects.get(id=pk)
        return JsonResponse(
            service, encoder = ServiceEncoder, safe=False
        )
    else:
        count, _ = Service.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})