# Generated by Django 4.0.3 on 2022-09-14 17:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0004_remove_service_status_delete_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.PositiveSmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=10)),
            ],
            options={
                'verbose_name_plural': 'statuses',
                'ordering': ('id',),
            },
        ),
        migrations.AddField(
            model_name='service',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='services', to='service_rest.status'),
        ),
    ]
