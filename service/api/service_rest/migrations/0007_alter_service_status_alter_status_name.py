# Generated by Django 4.0.3 on 2022-09-14 17:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0006_alter_status_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='service', to='service_rest.status'),
        ),
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(max_length=10, unique=True),
        ),
    ]
