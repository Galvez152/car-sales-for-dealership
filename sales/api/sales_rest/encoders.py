from common.json import ModelEncoder
from .models import Customer,AutomobileVO,SalesEmployee,SaleRecord
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties=[
        "name",
        "address",
        "phone"
    ]
class AutoVoEncoder(ModelEncoder):
    model=AutomobileVO
    properties=[
        "import_href",
        "vin",
        "color",
        "year",
        "sold",
        "model",
        "manufacturer"
    ]
class EmployeeEncoder(ModelEncoder):
    model=SalesEmployee
    properties=[
        "name",
        "employee_num"
    ]
class SaleEncoder(ModelEncoder):
    model=SaleRecord
    properties=[
        "automobileSold",
        "employeeSold",
        "customerSold",
        "salePrice",
        
    ]
    encoders = {
        "automobileSold": AutoVoEncoder(),
        "employeeSold": EmployeeEncoder(),
        "customerSold":CustomerEncoder()
    }
