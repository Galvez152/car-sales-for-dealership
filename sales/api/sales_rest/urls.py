from django.urls import path
from .views import api_AutoVO,api_customers,api_employees,api_Sales,api_AutoVOS,api_AutoInStock

urlpatterns = [
    path(
        "auto/",
        api_AutoVO,
        name="api_automobiles",
    ),
    path("automobiles/<str:vin>/", api_AutoVOS, name="api_AutoVOS"),
    path("instock/",api_AutoInStock, name="api_AutoInStock"),
    path(
        "customer/",api_customers,name="api_customer",
    ),
    path(
        "employee/",api_employees,name="api_employees"

    ),
    path(
        "sales/",api_Sales,name="api_Sales"

    )

]