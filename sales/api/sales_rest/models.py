

from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True,null=True)
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    model=models.CharField(max_length=50,null=True)
    manufacturer=models.CharField(max_length=50,null=True)
    sold=models.BooleanField(default=False)
    def __str__(self):
        return self.vin
    



class SalesEmployee(models.Model):
    name=models.CharField(max_length=200)
    employee_num=models.CharField(max_length=10, unique=True)
    def __str__(self):
        return self.name

class Customer(models.Model):
    name=models.CharField(max_length=200)
    address=models.CharField(max_length=200)
    phone=models.CharField(max_length=10)
    def __str__(self):
        return self.name

class SaleRecord(models.Model):
    automobileSold=models.ForeignKey(
        AutomobileVO,
        related_name="saleRecord",
        on_delete=models.CASCADE,
        )
    employeeSold=models.ForeignKey(
        SalesEmployee,
        related_name="saleRecord",
        on_delete=models.CASCADE
    )
    customerSold=models.ForeignKey(
        Customer,
        related_name="saleRecord",
        on_delete=models.CASCADE,
    )
    salePrice=models.IntegerField()
# Create your models here.
