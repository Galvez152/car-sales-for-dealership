import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()
from  sales_rest.models import AutomobileVO

# Import models from hats_rest, here.
# from hats_rest.models import Something
def get_automobiles():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    
    
    for autos in content["autos"]:
       
        AutomobileVO.objects.update_or_create(
            import_href=autos["href"],
            defaults={"color": autos["color"],"year":autos["year"],"vin":autos["vin"],"model":(autos["model"]["name"]),"manufacturer":(autos["model"]["manufacturer"]["name"])},
            
        )
def poll():
    while True:
        print('now auto poller polling for data')
        try:
            get_automobiles()

            
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
