import React from 'react';


class AutomobileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: '',
      year: '',
      vin: '',
      model_id:'',
      models:[],
      

    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
   
  }

  async componentDidMount() {
    
    let url = 'http://localhost:8100/api/models/';
    let response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      this.setState({ models: data.models});
    }


  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.models;
   


    const CarUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(CarUrl, fetchConfig);
    if (response.ok) {
      const newCar = await response.json();
      console.log(newCar);
      this.setState({
        color: '',
        year: '',
        vin: '',
        model_id:'',
      });
    }
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
    console.log(newState)
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new automobile</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
            <div className="mb-3">
                <select onChange={this.handleChange} required name="model_id" id="model_id" className="form-select">
                  <option value="model_id">select a model</option>
                  {this.state.models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="color" required type="text" value={this.state.color}name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="year" required type="number" value={this.state.year}name="year" id="year" maxLength="4" size="4" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
                <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="vin" required type="text" value={this.state.vin}name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;
