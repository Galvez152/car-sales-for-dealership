import React from "react";

export function serviceDate(dateTime) {
    const date = new Date(dateTime)
    const month = date.getUTCMonth()+1;
    const day = date.getUTCDate();
    const year = date.getFullYear();
    let setMonth = month;
    let setDay = day;
    if (month < 10) {
        setMonth = '0'+month
    }
    if (day < 10) {
        setDay = '0'+day
    }
    return (
        <div>
            {setMonth}-{setDay}-{year}
        </div>
    )
}

export function serviceTime(dateTime) {
    const time = new Date(dateTime)
    const hour = time.getUTCHours();
    let setHour = hour
    if (hour === 0) {
        setHour = 12
    } else if (hour > 12) {
        setHour -= 12
    }
    let minute = time.getMinutes();
    if (minute < 10) {
        minute = '0'+minute
    }
    if (hour < 12) {
        return (
            <div>
                {setHour}:{minute} AM
            </div>
        )
    }
    return (
        <div>
            {setHour}:{minute} PM
        </div>
    )
}