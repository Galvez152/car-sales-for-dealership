import React from "react";

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            name: '',
            employeeNumber: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        try {
            const url = 'http://localhost:8080/api/technicians/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
            } else {
                throw (new Error("Response Error", response));
            }
        } catch (err) {
            console.log(err);
    }}
    handleChange(event) {
        const newState = {};
        newState[event.target.id] = event.target.value;
        this.setState(newState);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.employee_number = data.employeeNumber;
        delete data.employeeNumber;
        const submitUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(submitUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            const cleared = {
                name: '',
                employeeNumber: '',
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Technician name" required type="text" name="name" id="name"/>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.employeeNumber} placeholder="Employee number (ex: 12345)" required type="text" name="employeeNumber" id="employeeNumber"/>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default TechnicianForm;