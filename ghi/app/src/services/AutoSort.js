export function sortTable(n) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById('listTable');
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i+1].getElementsByTagName("TD")[n];
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
            switching = true;
        } 
    }
}

export function sortTableTime(n1, n2) {
    let table, rows, switching, i, x1, x2, y1, y2, shouldSwitch;
    table = document.getElementById('listTable');
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length -1); i++) {
            shouldSwitch = false;
            x1 = rows[i].getElementsByTagName("TD")[n1];
            y1 = rows[i+1].getElementsByTagName("TD")[n1];
            x2 = rows[i].getElementsByTagName("TD")[n2];
            y2 = rows[i+1].getElementsByTagName("TD")[n2];
            if (x1.innerHTML.toLowerCase() === y1.innerHTML.toLowerCase()) {
                if (x2.innerHTML.toLowerCase().at(-8) === 'p' && y2.innerHTML.toLowerCase().at(-8) === 'a') {
                    shouldSwitch = true;
                    
                    break;
                } else if (x2.innerHTML.toLowerCase().at(-8) === y2.innerHTML.toLowerCase().at(-8)) {
                    if (y2.innerHTML.at(6) === '9' || y2.innerHTML.at(6) === '9') {
                        if (x2.innerHTML.slice(5, 7) === '11') {
                            shouldSwitch = true;
                            break;
                        }
                    } else {
                        if (x2.innerHTML.toLowerCase() > y2.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    }   
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
            switching = true;
        }
    }
}