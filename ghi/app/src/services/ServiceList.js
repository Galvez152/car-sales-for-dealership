import React from "react";
import { serviceDate, serviceTime} from './DateTime';
import { sortTable, sortTableTime } from './AutoSort';

class ServiceList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            services:[],
        }
        this.finishService = this.finishService.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/api/services/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const finishedTrue = []
            for (let i in data.services) {
                if (!data.services[i].finished) {
                    finishedTrue.push(data.services[i])
                    this.setState({ services: finishedTrue });
                }
            }
        }
    }
    async finishService(id, name, date, reason, vin, technician) {
        await fetch(`http://localhost:8080/api/services/${id}` ,{
            method: 'put',
            body: JSON.stringify({
                customer_name: name,
                date_time: date,
                reason: reason,
                vin: vin,
                technician: technician,
                finished: true    
            }),
            headers: {
                'Content-Type': 'application/json',
            }
        }).then((result)=>{
            result.json().then((resp)=>{
            })
        })
        const url = 'http://localhost:8080/api/services/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const finishedTrue = []
            for (let i in data.services) {
                if (!data.services[i].finished) {
                    finishedTrue.push(data.services[i])
                    this.setState({ services: finishedTrue });
                    }
                }
            }
        }
    render() {
        return (
            <div>
                <h1>Service appointments</h1>
                <table className="table table-striped" id="listTable">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Cancel</th>
                            <th>Finish</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.services.map(service => {
                            const dateTime = service.date_time;
                            return (
                                <tr key={service.id}>
                                    <td>{service.vin.vin}</td>
                                    <td>{service.customer_name}</td>
                                    <td onLoad={sortTable(2)}>{serviceDate(dateTime)}</td>
                                    <td onLoad={sortTableTime(2, 3)} id="tableTime">{serviceTime(dateTime)}</td>
                                    <td>{service.technician.name}</td>
                                    <td>{service.reason}</td>
                                    <td>
                                        <a className="btn btn-danger" href={`cancel/${service.id}`}>X</a>
                                    </td>
                                    <td>
                                        <button type="button" className="btn btn-success" onClick={()=>this.finishService(
                                            service.id, service.customer_name, service.date_time, service.reason, service.vin.vin, service.technician.name
                                            )}>✓</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <div>
                </div>
            </div>
        );
    }
}

export default ServiceList;