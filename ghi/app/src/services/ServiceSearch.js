import React from "react";
import { serviceDate } from './DateTime';
import { handleFinished } from './SymbolFunctions';

class ServiceSearch extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            vinOrName: '',
            services:[],
            sortVinArrow: '',
            sortCustomerArrow: '',
            sortDateTimeArrow: '',
            sortTechnicianArrow: '',
            sortReasonArrow: '',
            sortFinishedArrow: '',

        }
        this.handleFilter = this.handleFilter.bind(this);
        this.sortTable = this.sortTable.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/api/services/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ services: data.services });
        }
    }
    async handleFilter(event) {
        event.preventDefault();
        const searchFilter = {};
        const servicesFiltered = [];
        const services = this.state.services;
        searchFilter[event.target.id] = event.target.value;
        for (let i = 0; i < services.length; i++ ) {
            if (services[i]['customer_name'].toUpperCase().includes(searchFilter['vinOrName'].toUpperCase()) || services[i]['vin']['vin'].includes(searchFilter['vinOrName'].toUpperCase())
            
            ) {
                servicesFiltered.push(services[i])
            } else {
                this.setState({ services: []});
            }
        }
        this.setState({ services: servicesFiltered });
        if (searchFilter['vinOrName'] === '') {
            const url = 'http://localhost:8080/api/services/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ services: data.services });
            }
        }
    }
    sortTable(n) {
        let table, rows, switching, i, x, y, shouldSwitch, dir = 'asc', switchCount = 0;
        table = document.getElementById('searchTable');
        switching = true;

        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i+1].getElementsByTagName("TD")[n];
                if (dir === 'asc') {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir === 'desc') {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
                switching = true;
                switchCount ++;
            } else {
                if (switchCount === 0 && dir === 'asc') {
                    dir = 'desc';
                    switching = true;
                }
            }
        }
        this.setState({
            sortVinArrow: '',
            sortCustomerArrow: '',
            sortDateTimeArrow: '',
            sortTechnicianArrow: '',
            sortReasonArrow: '',
            sortFinishedArrow: '',
        })
        if (dir === 'asc') {
            if (n === 0) {
                this.setState({
                    sortVinArrow: '⏶',
                })
            } else if (n === 1) {
                this.setState({
                    sortCustomerArrow: '⏶',
                })
            } else if (n === 2) {
                this.setState({
                    sortDateTimeArrow: '⏶',
                })
            } else if (n === 3) {
                this.setState({
                    sortTechnicianArrow: '⏶',
                })
            } else if (n === 4) {
                this.setState({
                    sortReasonArrow: '⏶',
                })
            } else if (n === 5) {
                this.setState({
                    sortFinishedArrow: '⏶',
                })
            }
        } else if (dir === 'desc') {
            if (n === 0) {
                this.setState({
                    sortVinArrow: '⏷',
                })
            } else if (n === 1) {
                this.setState({
                    sortCustomerArrow: '⏷',
                })
            } else if (n === 2) {
                this.setState({
                    sortDateTimeArrow: '⏷',
                })
            } else if (n === 3) {
                this.setState({
                    sortTechnicianArrow: '⏷',
                })
            } else if (n === 4) {
                this.setState({
                    sortReasonArrow: '⏷',
                })
            } else if (n === 5) {
                this.setState({
                    sortFinishedArrow: '⏷',
                })
            }
        }
    }
    render() {
        return (
            <div>
                <form className="d-flex mt-3">
                    <input className="form-control me-2" required type="search" placeholder="VIN number or Customer Name" aria-label="Search" name="vinOrName" id="vinOrName" onChange={this.handleFilter}/>
                </form>
                <h1 className="mt-3">Search service appointments</h1>
                <table className="table table-striped table-sm" id="searchTable">
                    <thead>
                        <tr>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(0)}>VIN{this.state.sortVinArrow}</th>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(1)}>Customer name{this.state.sortCustomerArrow}</th>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(2)}>Date{this.state.sortDateTimeArrow}</th>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(3)}>Technician{this.state.sortTechnicianArrow}</th>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(4)}>Reason{this.state.sortReasonArrow}</th>
                            <th role="button" className="border text-success" onClick={()=> this.sortTable(5)}>Finished?{this.state.sortFinishedArrow}</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.services.map(service => {
                        const dateTime = service.date_time
                        return (
                            <tr key={service.id}>
                                <td>{service.vin.vin}</td>
                                <td>{service.customer_name}</td>
                                <td>{serviceDate(dateTime)}</td>
                                <td>{service.technician.name}</td>
                                <td>{service.reason}</td>
                                <td className="text-center">{handleFinished(service.finished)}</td>
                            </tr> 
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ServiceSearch;