import React from "react";

class ServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            customerName: '',
            date: '',
            time: '',
            reason: '',
            vin: '',
            technician: '',
            technicians: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        try {
            const url = 'http://localhost:8080/api/technicians/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ technicians: data.technicians })
            } else {
                throw (new Error("Response Error", response));
            }
        } catch (err) {
            console.log(err);
    }}
    handleChange(event) {
        const newState = {};
        newState[event.target.id] = event.target.value;
        this.setState(newState);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.customer_name = data.customerName;
        data.date_time = data.date+'T'+data.time;
        delete data.date;
        delete data.time;
        delete data.customerName;
        delete data.technicians
        const submitUrl = 'http://localhost:8080/api/services/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(submitUrl, fetchConfig);
        if (response.ok) {
            const newService = await response.json();
            const cleared = {
                customerName: '',
                date: '',
                time: '',
                reason: '',
                vin: '',
                technician: '',
                technicians: [],
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a service appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-service-form">
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.customerName} placeholder="Customer name" required type="text" name="customerName" id="customerName"/>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date"/>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.time} placeholder="Time" required type="time" name="time" id="time"/>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.reason} placeholder="Reason for appointment" required type="text" name="reason" id="reason"/>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} placeholder="VIN (ex: 1X1XX1XX1XX123456)" required type="text" name="vin" id="vin"/>
                            </div>
                            <div className="form-group row mb-3">
                                <select onChange={this.handleChange} value={this.state.technician} required id="technician" name="technician">
                                    <option>Select a technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.name}>{technician.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceForm;