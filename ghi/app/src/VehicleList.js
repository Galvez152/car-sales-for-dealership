import React from "react";
import {NavLink} from 'react-router-dom'
import { sortTable } from './services/AutoSort'

class VehicleList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            models:[],
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models });
        }
    }

    render() {
        return (
            <div>
                <h1>Vehicle models</h1>
                <table className="table table-striped" id="listTable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td onLoad={sortTable(1)}>{model.manufacturer.name}</td>
                                    <td><img className="vehicle-img" src={model.picture_url} alt={model.name}/></td>
                                    <style>{".vehicle-img{max-width:250px;}"}</style>
                                </tr>
                            );
                        })}

                    </tbody>
                </table>
                <div>
                    <NavLink className="btn btn-primary" to="/vehicles/new">Create a vehicle</NavLink>
                </div>
            </div>
        );
    }

}

export default VehicleList;