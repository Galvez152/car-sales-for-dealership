import React from "react";
import {NavLink} from 'react-router-dom'
import { sortTable } from './services/AutoSort';

class AutomobileList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            autos:[],
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ autos: data.autos });
        }
    }

    render() {
        return (
            <div>
                <h1>Automobiles in inventory</h1>
                <table className="table table-striped" id="listTable">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Year</th>
                            <th>Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map(auto => {
                            return (
                                <tr key={auto.id}>
                                    <td>{auto.vin}</td>
                                    <td onLoad={sortTable(1)}>{auto.model.manufacturer.name}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.color}</td>
                                </tr>
                            );
                        })}

                    </tbody>
                </table>
                <div>
                    <NavLink className="btn btn-primary" to="/automobiles/new">Add an automobile</NavLink>
                </div>
            </div>
        );
    }

}

export default AutomobileList;