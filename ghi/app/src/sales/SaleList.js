import React from "react";
import {NavLink} from 'react-router-dom'

class SaleList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            
            choice:'all',
            employee:[],
            automobileSold:[],
            filterArray:[]
        }

    
    this.handleChange = this.handleChange.bind(this);
    this.handleFilter=this.handleFilter.bind(this);
   
    
  
   
    }

    handleChange(event) {
        console.log(event.target.value)
        const newState = {};
        newState[event.target.id] = event.target.value;
        console.log(newState)
        this.setState(newState);
        console.log(newState)
      }

        

      async handleFilter(event){
        let url = 'http://localhost:8090/api/sales/';
        let response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ automobileSold: data.Sale});
            console.log(this.state.automobileSold)
            
        }
        if(event.target.value!=='all'){
            console.log("its reaching here ")
            const newState = {};
            newState[event.target.id] = event.target.value;
            this.setState(newState)
            
           
            
            const filtered =this.state.automobileSold.filter(item => {
                return item.employeeSold.name === newState[event.target.id];
            });
            this.setState({ filterArray: filtered});
        }
    
     
       

    }


  

    
    async componentDidMount() {
        let url = 'http://localhost:8090/api/sales/';
        let response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ filterArray: data.Sale});
            this.setState({ automobileSold: data.Sale});
        }
        url = 'http://localhost:8090/api/employee/';
        response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
    
          this.setState({ employee: data.employee});
        }
    }

 


    render() {
        return (
            <div>
                <h1>Automobiles in inventory</h1>
                <div className="dropup-center dropdown">
                    <button  className="btn btn-secondary dropdown-toggle" type="button" value={this.state.choice}data-bs-toggle="dropdown" aria-expanded="false">
                    Currently Viewing: {this.state.choice}
                    </button>
                    
                    <ul  className="dropdown-menu" id="choice">
                    <li><button className="dropdown-item" onClick={this.handleFilter}  id="choice" value="all" >View All</button></li>
                        {this.state.employee.map(item=> {
                            return (
                                    
                                    <li  key={item.id}><button onClick={this.handleFilter}  id="choice" value={item.name}className="dropdown-item"   type="button">{item.name}</button></li>
                                
                            );
                        })}
                    </ul>
                    </div>
                <table className="table table-striped">
                    <thead>
                        
                        <tr>
                            <th>employee Sold</th>
                            <th>customer Sold</th>
                            <th>VIN</th>
                            <th>Amount</th>
                            
                          
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.filterArray.map(item=> {
                            return (
                                <tr key={item.id}>
                                    <td>{item.employeeSold.name}</td>
                                    <td>{item.customerSold.name}</td>
                                    <td>{item.automobileSold.vin}</td>
                                    <td>{item.salePrice}</td>
                                
                                </tr>
                            );
                        })}

                    </tbody>
                </table>
                <div>
                    <NavLink className="btn btn-primary" to="/automobiles/new">Add an automobile</NavLink>
                </div>
            </div>
        );
    }


}

export default SaleList;