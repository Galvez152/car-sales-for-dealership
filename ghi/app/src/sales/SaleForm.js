import React from 'react';

class SaleForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobileSold: '',
      employeeSold: '',
      customerSold: '',
      salePrice: '',
      customers:[],
      employee:[],
      cars:[],
      carsToBuy:[]

    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
   
  }

  async componentDidMount() {
    
    let url = 'http://localhost:8090/api/customer/';
    let response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      
      this.setState({ customers: data.customer});
    }
    url = 'http://localhost:8090/api/employee/';
    response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
     
      this.setState({ employee: data.employee});
    }
    url = 'http://localhost:8090/api/auto/';
    response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
     
      this.setState({cars: data.AutomobileVO});
    }
    const filtered =this.state.cars.filter(car => {
      return car.sold === false;
  });

    this.setState({carsToBuy: filtered});

  }
  
  async handleSubmit(event) {
    event.preventDefault();
    const data = {
      automobileSold:this.state.automobileSold,
      employeeSold:this.state.employeeSold,
      customerSold:this.state.customerSold,
      salePrice:this.state.salePrice,
  };
    

    const SaleUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(SaleUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);
   
      this.setState({
        automobileSold: '',
        employeeSold: '',
        customerSold: '',
        salePrice: '',
      });
    }
    const soldUnit={sold:true}
    const AutoUrl = `http://localhost:8090${data.automobileSold}`;
    const Config = {
      method: "put",
      body: JSON.stringify(soldUnit),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const result = await fetch(AutoUrl, Config);
    console.log(result)
  }
  
  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
    console.log(newState)
  }
  

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
            <div className="mb-3">
                <select onChange={this.handleChange} required name="customerSold" id="customerSold" className="form-select">
                  <option value="customerSold">Select Customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.name}>{customer.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} required name="employeeSold" id="employeeSold" className="form-select">
                  <option value="employeeSold">Choose Sale Employee </option>
                  {this.state.employee.map(seller => {
                    return (
                      <option key={seller.employee_num} value={seller.employee_num}>{seller.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} required name="automobileSold" id="automobileSold" className="form-select">
                  <option value="automobileSold">Car Sold </option>
                  {this.state.carsToBuy.map(car => {
                    return (
                      <option key={car.import_href} value={car.import_href}>{car.vin}</option>
                    )
                  })}
                </select>
              </div>
                  <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="salePrice" required type="number" name="salePrice" id="salePrice" className="form-control" />
                <label htmlFor="salePrice">Car Sold Price</label>
              </div>
              <button  id="liveBtn" onClick={this.handleSuccess}className="btn btn-primary">Create</button>
              
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleForm;