import React from 'react';
class EmployeeForm extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        name:'',
        employee_num:'',
       
      };
  
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      async handleSubmit(event) {
        event.preventDefault();
        const data = {
          name:this.state.name,
          employee_num:this.state.employee_num,
    
      };
        
          const EmployeeUrl = 'http://localhost:8090/api/employee/';
          const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(EmployeeUrl, fetchConfig);
          if (response.ok) {
            const newEmployee = await response.json();
            console.log(newEmployee);
            const cleared = {
              name: '',
              employee_num:''
              
            };
            this.setState(cleared);
          }
        }
        handleChange(event) {
          const newState = {};
          newState[event.target.id] = event.target.value;
          this.setState(newState);
          console.log(newState)
        }
    
     
     
      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>New Sale Employee</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                  <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name} />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input onChange={this.handleChange} placeholder="employee_num" required type="text" name="employee_num" id="employee_num" maxLength="10" size="10" className="form-control" value={this.state.employee_num} />
                    <label htmlFor="employee_num">employee number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }
  export default EmployeeForm;